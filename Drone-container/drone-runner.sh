#!/bin/bash

docker run -d \
  --network akalan \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DRONE_RPC_PROTO=http \
  -e DRONE_RPC_HOST="192.168.10.10:9880" \
  -e DRONE_RPC_SECRET="3ab0479b9718d8337e111796c445eefe" \
  -e DRONE_RUNNER_CAPACITY=4 \
  -e DRONE_RUNNER_NAME="runner" \
  -p 3000:3000 \
  --restart always \
  --name runner \
  drone/drone-runner-docker:1
