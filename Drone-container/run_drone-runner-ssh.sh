#!/bin/bash

docker run -d \
  -e DRONE_RPC_PROTO=http \
  -e DRONE_RPC_HOST=192.168.10.10:9880 \
  -e DRONE_RPC_SECRET=3ab0479b9718d8337e111796c445eefe \
  -p 4000:3000 \
  --restart always \
  --name runner-ssh \
  drone/drone-runner-ssh
