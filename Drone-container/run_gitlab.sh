
#!/bin/bash

docker run \
       --privileged \
       --detach \
       --network akalan \
       --hostname 192.168.10.96 \
       --env GITLAB_OMNIBUS_CONFIG="external_url 'http://192.168.10.96:9080/';" \
       --publish 8443:443 --publish 9080:80 --publish 8022:22 \
       --name gitlab1 \
       --restart always \
       --volume /srd/gitlab/config:/etc/gitlab \
       --volume /srd/gitlab/logs:/var/log/gitlab \
       --volume /srd/gitlab/data:/var/opt/gitlab \
       gitlab/gitlab-ce:latest
