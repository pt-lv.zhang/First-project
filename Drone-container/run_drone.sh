#!/bin/bash

docker run \
       -d \
       --network akalan \
       --volume=/var/lib/drone:/data \
       --volume=/var/run/docker.sock:/var/run/docker.sock \
       --env=DRONE_AGENTS_ENABLED=true \
       --env=DRONE_GITLAB_SERVER=http://192.168.10.10:9080 \
       --env=DRONE_GITLAB_CLIENT_ID="7c1d3cc9f6a1f936f7814a34ab5028864aacbb047846295471677a73a2db6d74" \
       --env=DRONE_GITLAB_CLIENT_SECRET="ac87fd836cac4c87b979d6de69dbddf15147eedeadd239038e71d2e2beeef48d" \
       --env=DRONE_GIT_ALWAYS_AUTH=true \
       --env=DRONE_USER_CREATE=username:zhang,admin:true \
       --env=DRONE_SERVER_HOST="192.168.10.10:9880" \
       --env=DRONE_SERVER_PROTO=http \
       --env=DRONE_RPC_SECRET="3ab0479b9718d8337e111796c445eefe" \
       --publish=9880:80 \
       --publish=1080:443 \
       --restart=always \
       --name=drone \
       drone/drone:latest
